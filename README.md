SpringCloud Demo

# LiaWan-Cloud

如果你准备开始学习 SpringCloud，建议先阅读下文【**适用人群**】，看看是否适合你。有任何问题都可以联系公众号：JavaPub

**维护人**：公众号 JavaPub

[toc]

#### 介绍

倆萬 Cloud 版，基于Spring Boot、Spring Cloud & Alibaba、nacos + gateway + feign + hystrix。提供一个开箱即学的入门项目

##### 适用人群

本项目是我在学习期间的一个实践，致力于让每一个想了解和学习 SpringCloud 的爱好者可以快速了解这项技术。项目计划配备几篇对应文章，帮助更快上手。建议收藏，有空余时间会尽快更新。

**维护人**：公众号 JavaPub

#### 解决痛点 

1. SpringBoot的发展确实解决了我们大部分关于版本的痛点，但是第一次搭建一整套SpringCloud&alibaba并不是一件容易的事
2. 各种组件的整合，到底需要哪些依赖，怎么玩转

**维护人**：公众号 JavaPub

#### 软件架构

软件架构说明

这是一个 SpringCloud Demo，展示了相关调用链

![在这里插入图片描述](https://img-blog.csdnimg.cn/1028b6b5427142429fec199bf455cfbe.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBASmF2YVB1Yi1yb2RlcnQ=,size_20,color_FFFFFF,t_70,g_se,x_16)


#### 安装教程

1. 安装Nacos

    ```
    下载解压启动
    bin/startup.sh -m standalone
    访问
    ip:8848/nacos/#/login
    ```

2. 启动服务
    
    ```
    liawan-gatewsy
    liawan-api
    liawan-user
    ```
    
    不分先后顺序。每个服务下有对应的 README.md 说明文档。
    

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)


