package com.liawan.user.liawanuser.controller;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: JavaPub
 * @License: https://github.com/Rodert/
 * @Contact: https://javapub.blog.csdn.net/
 * @Date: 2022/1/1 14:52
 * @Version: 1.0
 * @Description:
 */

@RestController
@RefreshScope
public class UserController {

    @RequestMapping("/user")
    public String hello(){
        return "Hello Welcome To Visit liawan-user";
    }

}
