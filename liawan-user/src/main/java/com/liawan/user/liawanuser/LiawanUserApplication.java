package com.liawan.user.liawanuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: JavaPub
 * @License: https://github.com/Rodert/
 * @Contact: https://javapub.blog.csdn.net/
 * @Date: 2022/1/1 14:52
 * @Version: 1.0
 * @Description:
 */

@SpringBootApplication
public class LiawanUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiawanUserApplication.class, args);
    }

}
