liawan-gateway

网关服务 

[toc]

## 前言

这里演示 gateway 使用方法。项目记得 **start**，后面会持续完善架构及功能，从0到1。

## Demo测试

1. 启动 main 方法

    启动 gateway 、 api (不分先后顺序)

2. 测试匹配路由

    http://127.0.0.1:6020/?url=baidu
    
    http://127.0.0.1:6020/?url=qq

    ![在这里插入图片描述](https://img-blog.csdnimg.cn/043f21c804ad44dea3c1f116b8413815.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBASmF2YVB1Yi1yb2RlcnQ=,size_20,color_FFFFFF,t_70,g_se,x_16)

    ![在这里插入图片描述](https://img-blog.csdnimg.cn/492f43c7ed5f4785b91cfa257d33b396.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBASmF2YVB1Yi1yb2RlcnQ=,size_20,color_FFFFFF,t_70,g_se,x_16)

3. 测试动态路由

    http://127.0.0.1:6020/liawan-api/api/hello

    通过请求 liawan-gateway 网关服务，请求到 liawan-api 服务

    ![在这里插入图片描述](https://img-blog.csdnimg.cn/3fb545809fa44eaea84c76c48aeb8397.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBASmF2YVB1Yi1yb2RlcnQ=,size_20,color_FFFFFF,t_70,g_se,x_16)

4. 远程调用openfeign

    http://127.0.0.1:6020/liawan-api/api/getHello
    
    ![在这里插入图片描述](https://img-blog.csdnimg.cn/fc82c3fa07204f7c9c271e57bdf461c3.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBASmF2YVB1Yi1yb2RlcnQ=,size_20,color_FFFFFF,t_70,g_se,x_16)
