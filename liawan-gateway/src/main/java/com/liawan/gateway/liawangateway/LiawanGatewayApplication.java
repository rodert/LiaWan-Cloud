package com.liawan.gateway.liawangateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author: JavaPub
 * @License: https://github.com/Rodert/
 * @Contact: https://javapub.blog.csdn.net/
 * @Date: 2022/1/1 14:52
 * @Version: 1.0
 * @Description:
 */

@SpringBootApplication
@EnableDiscoveryClient
public class LiawanGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiawanGatewayApplication.class, args);
    }
    
}
