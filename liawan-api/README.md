liawan-api

api服务

[toc]

## 前言

一个普通的api服务，给 gateway 测试使用

1. 启动 main 方法

2. 测试api调用

    http://127.0.0.1:6010/api/hello

    ![在这里插入图片描述](https://img-blog.csdnimg.cn/557c182a65ee4ed7b106ac3ebb42cc43.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBASmF2YVB1Yi1yb2RlcnQ=,size_20,color_FFFFFF,t_70,g_se,x_16)

    **openfeign 调用 liawan-user**
    
    http://127.0.0.1:6010/api/getHello
    
    ![在这里插入图片描述](https://img-blog.csdnimg.cn/277026a883914956ad1c8a7c9775d278.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBASmF2YVB1Yi1yb2RlcnQ=,size_20,color_FFFFFF,t_70,g_se,x_16)
