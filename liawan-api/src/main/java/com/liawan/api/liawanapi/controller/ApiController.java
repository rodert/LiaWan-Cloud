package com.liawan.api.liawanapi.controller;

import com.liawan.api.liawanapi.service.openfeign.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: JavaPub
 * @License: https://github.com/Rodert/
 * @Contact: https://javapub.blog.csdn.net/
 * @Date: 2022/1/1 14:52
 * @Version: 1.0
 * @Description:
 */

@RestController
@RequestMapping("/api")
public class ApiController {

    @Resource
    private UserService userService;

    @RequestMapping("/hello")
    public String hello() {
        return "hello liawan-gateway to liawan-api";
    }

    /**
     * 请求openfeign
     * @return
     */
    @RequestMapping("/getHello")
    public String getHello() {
        return userService.getHello();
    }

}
