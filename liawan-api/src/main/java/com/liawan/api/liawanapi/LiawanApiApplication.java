package com.liawan.api.liawanapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author: JavaPub
 * @License: https://github.com/Rodert/
 * @Contact: https://javapub.blog.csdn.net/
 * @Date: 2022/1/1 14:52
 * @Version: 1.0
 * @Description:
 */

@EnableDiscoveryClient
@EnableFeignClients
//@MapperScan("com.liawan.liawanuser.mapper")
@RefreshScope
@SpringBootApplication
public class LiawanApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiawanApiApplication.class, args);
    }

}
