package com.liawan.api.liawanapi.service.openfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: JavaPub
 * @License: https://github.com/Rodert/
 * @Contact: https://javapub.blog.csdn.net/
 * @Date: 2022/1/1 14:52
 * @Version: 1.0
 * @Description:
 */

@Component
@FeignClient("liawan-user")
public interface UserService {

    @RequestMapping("/user")
    public String getHello();

}
